import bank
import Notify

Notify.init("pybank_notification")

print("App name is " + Notify.get_app_name())

n = Notify.Notification("test notification", "Test notification from pybank", None, None)
n.set_timeout(5000)
n.show()

Notify.uninit()
