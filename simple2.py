import sys

import bank
import GdkPixbuf

filename = sys.argv[1]
p = GdkPixbuf.Pixbuf.new_from_file(filename)
print 'Size of %s is %dx%d' % (filename, p.get_width(), p.get_height())
