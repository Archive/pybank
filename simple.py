import os
import sys

import bank
import GLib
import Poppler

def main(args):
    if len(args) < 2:
        print 'Usage: %s filename' % args[0]
        return 1

    filename = args[1]
    uri = 'file://' + os.path.abspath(filename)
    doc = Poppler.Document.new_from_file(uri, '')
    print doc
    pages = doc.get_n_pages()
    page = doc.get_page(0)
    print page
    #print page.get_size()

    print 'Document %s has %d pages' % (filename, pages)

if __name__ == '__main__':
    sys.exit(main(sys.argv))
