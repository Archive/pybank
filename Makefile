PCDEPS=glib-2.0 gobject-2.0 gmodule-2.0 gthread-2.0 gobject-introspection-1.0 pygobject-2.0
IR_CFLAGS=-g -Wall -I$(IRPATH) `pkg-config --cflags $(PCDEPS)`
IR_LIBS=-lffi `pkg-config --libs $(PCDEPS)`

PY_VERSION=2.5
PYTHON=python2.5
PY_INCLUDE=`$(PYTHON) -c 'from distutils import sysconfig; print sysconfig.get_python_inc()'`
PY_CFLAGS=-I$(PY_INCLUDE) `pkg-config --cflags pygobject-2.0`
PY_LIBS=#-lpython$(PY_VERSION)

BANK_SOURCES =			\
	bank/bank.c		\
	bank/bank-argument.c	\
	bank/bank-info.c	\
	bank/bank-repository.c

all: bank/repo.so

bank/repo.so: $(BANK_SOURCES)
	gcc -O0 -fPIC -shared -o bank/repo.so $(BANK_SOURCES) $(IR_CFLAGS) $(IR_LIBS) $(PY_CFLAGS) $(PY_LIBS)

clean:
	@rm -f bank/repo.so

