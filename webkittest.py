import bank

import GLib
import Gtk
import WebKit

GLib.thread_init(None)

win = Gtk.Window(Gtk.WindowType.TOPLEVEL)
win.connect('delete-event', lambda *args: Gtk.main_quit())

sw = Gtk.ScrolledWindow(None, None)
win.add(sw)

p = WebKit.WebView()
p.open("http://www.google.com/")
sw.add(p)

win.set_size_request(640, 480)
win.show_all()

Gtk.main()
